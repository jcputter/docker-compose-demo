# docker-compose-demo

Simple Go webapp using the standard http library.

The Dockerfile uses multi-stage for building the app with **golang:1.16-alpine** as the builder image and **scratch** for runtime. The result is a very minimal container (about 6mb)

This repo has been configured to build and push a refresh docker image on every commit the main branch

Info about the scratch image see https://hub.docker.com/_/scratch 


**Build the runtime image** 

`docker build -t mukuru-http:latest .`

**Use docker-compose**

`docker-compose up`

The app will be locally accessible at http://localhost:8080

**Deploy to Kubernetes**

This will deploy 4 replica's of the Mukuru HTTP app and configure a ingress service to reach the app via https://localhost

*NB This has only been tested with Docker for Desktop (Kubernetes) (3.3.3)*

The deployment consists of a pod, service and ingress controller

**Set your kubectl context to Docker Desktop**

`kubectl config set current-context docker-desktop`

**Setup Nginx Ingress Controller**

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.47.0/deploy/static/provider/cloud/deploy.yaml`

*Note the SSL certifiate will be invalid as this is only for testing (Kubernetes Ingress Controller Fake Certificate)*

**Apply the manifest**

`kubectl apply -f deployment.yaml`

You should now be able to access the app via SSL on https://localhost


![mukuru-http](app.png)