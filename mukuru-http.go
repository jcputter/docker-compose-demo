package main

import (
	"fmt"
	"net/http"
	"os"
	"runtime"
)

func handler(w http.ResponseWriter, r *http.Request) {
	os.Setenv("VERSION", runtime.Version())
	fmt.Fprintf(w, "We're up and running!! Rocking Version: %v", os.Getenv("VERSION"))
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":3000", nil)
}
