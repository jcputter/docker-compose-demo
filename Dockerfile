FROM golang:1.16-alpine AS builder

ENV CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

LABEL maintainer="jcputter@gmail.com"
LABEL app="mukuru-http"

# Builds the project
WORKDIR /dist
COPY mukuru-http.go /dist
RUN go build mukuru-http.go

# This create a single layer container 
FROM scratch
COPY --from=builder /dist/mukuru-http /dist/mukuru-http 
ENTRYPOINT [ "/dist/mukuru-http" ]
EXPOSE 3000